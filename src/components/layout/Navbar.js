import React, { useEffect } from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import SearchBar from "./SearchBar";

const Navbar = () => {
  useEffect(() => {
    var elem = document.querySelector(".sidenav");
    M.Sidenav.init(elem, {
      edge: "left",
      inDuration: 250
    });
  }, []);

  return (
    <header>
      <nav className="blue">
        <div className="nav-wrapper container">
          <a href="#!" className="brand-logo">
            IT-Logger
          </a>
          <a href="#!" className="sidenav-trigger" data-target="mobile-nav">
            <i className="material-icons">menu</i>
          </a>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            <li>
              <a href="#add-log-modal" className="modal-trigger">
                <i className="material-icons left">add</i>
                Add Log
              </a>
            </li>
            <li>
              <a href="#add-tech-modal" className="modal-trigger">
                <i className="material-icons left">person_add</i>
                Add Tech
              </a>
            </li>
            <li>
              <a href="#tech-list-modal" className="modal-trigger">
                <i className="material-icons left">person</i>Get TechList
              </a>
            </li>
          </ul>

          <ul className="sidenav" id="mobile-nav">
            <div className="blue" style={{ padding: "20px" }}>
              <h4 className="center blue">IT-Logger</h4>
            </div>

            <li>
              <a href="#add-log-modal" className="modal-trigger">
                <i className="material-icons">add</i>
                Add Log
              </a>
            </li>
            <li>
              <a href="#add-tech-modal" className="modal-trigger">
                <i className="material-icons">person_add</i>
                Add Tech
              </a>
            </li>
            <li>
              <a href="#tech-list-modal" className="modal-trigger">
                <i className="material-icons">person</i>
                Get TechList
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <SearchBar />
    </header>
  );
};

export default Navbar;
