import React, { useRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { searchLogs } from "../../actions/logActions";

const SearchBar = ({ searchLogs }) => {
  const text = useRef("");

  const onChange = () => {
    searchLogs(text.current.value);
  };

  const clearText = () => {
    text.current.value = " ";
    searchLogs(text.current.value);
  };

  return (
    <nav style={{ marginBottom: "30px" }} className="white">
      <div className="nav-wrapper container">
        <form>
          <div className="input-field">
            <input
              id="search"
              type="search"
              ref={text}
              placeholder="Search Logs"
              required
              onChange={onChange}
              className="black-text"
            />
            <label className="label-icon " htmlFor="search">
              <i className="material-icons black-text">search</i>
            </label>
            <i className="material-icons" onClick={clearText}>
              close
            </i>
          </div>
        </form>
      </div>
    </nav>
  );
};

SearchBar.propTypes = {
  searchLogs: PropTypes.func.isRequired
};

export default connect(
  null,
  { searchLogs }
)(SearchBar);
